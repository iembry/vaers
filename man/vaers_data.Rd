% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data-vaers_data.R
\docType{data}
\name{vaers_data}
\alias{vaers_data}
\title{US Vaccine Adverse Event Reporting System (VAERS) data for 1990 - Present}
\format{

}
\usage{
vaers_data
}
\description{
A table containing "a detailed description of the data provided in each
field of the VAERSDATA.CSV file. The first two fields in this table are the
only fields of the dataset not derived from the the VAERS-1 form."
}
\references{
US Centers for Disease Control and Prevention (CDC) and the US Food and Drug Administration (FDA) Vaccine Adverse Event Reporting System (VAERS) \url{https://vaers.hhs.gov/} and \url{https://vaers.hhs.gov/docs/VAERSDataUseGuide_November2020.pdf}.
}
\keyword{datasets}
